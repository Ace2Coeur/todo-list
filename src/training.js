//////////////SOUMETTRE UN FORMULAIRE AVEC LA WEB API FORMDATA ET L'UTILISATION DE FETCH///////////////

// const form = document.querySelector('form');

// form.addEventListener('submit', event => {
//     event.preventDefault();
//     const formData = new FormData(form);

//     formData.append("name", "age");
//     formData.set("name", "age");

//     for (let pair of formData) {
//         console.log(pair);
//     }

//     fetch('/test', {
//         method: "POST",
//         body: formData
//     });
// });

//////////////CRÉER UNE URL AVEC DES QUERY STRINGS ET LA WEB API URL///////////////

// const url = new URL("https://wikipedia.fr/napoleon?key1=value&key2=value2");

// url.searchParams.append("key3", "value3"); //Ajouter un paramètre dans la query string
// url.searchParams.set("key4", "tom&jerry");